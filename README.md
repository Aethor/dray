# DRay

DRay is a real-time relationships graph visualizer based on Electron


# Quick Start

First, clone the repository :

```sh
git clone "https://gitlab.com/Aethor/dray.git"
cd dray
```

Then, install npm dependencies :

```sh
npm install 
```

You can now start DRay with :

```sh
npm start
```

Use *File -> Open...* to open an analyse file. An analyse file can be obtained using :

* [bert-quote-attribution](https://gitlab.com/Aethor/bert-quote-attribution.git), that can produce analyse of character relationships in novels
* [BirdSoup](https://gitlab.com/Aethor/birdsoup.git), that produces an analyse of politican relationships in time given their stances on a specific subject
* The cinema module of [DeltaRay](https://gitlab.com/Aethor/deltaray) (not recommended)


# Embedding in Another Project

Efforts were started to embed DRay correctly in another project by displaying any HTML div. However, this has not been achieved yet.

For now, the project can be launched as a separate window using the command line.

## Example

First, clone the project :

```sh
git clone "https://gitlab.com/Aethor/dray.git"
cd dray
```

Build it for the first time :

```sh
npm install
npx tsc
```

You can now call it on a specific file using the command line :

```sh
npx electron-forge start -- /path/to/file
```

To embed from another node application :

```javascript
var exec = require("child_process").exec;
const drayPath = "/path/to/dray";

function startSimulation(inputFilePath){
	exec(`npx electron-forge start -- ${inputFilePath}`, {cwd: drayPath}, (error, stdout, stderr) => {
		if (error) { console.log(error); return; }
		if (stderr) { console.log(stderr); return; }
		console.log(stdout);
	})
}
```
