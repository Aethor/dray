import { ipcRenderer } from 'electron';
import { Preferences } from './index';
import { startSimulation } from './graph/graph';
import { Analyse } from './analyseTypes';


ipcRenderer.on('load', (event, preferences: Preferences) => {
    if (preferences.darkTheme) {
	let darkThemeLink = document.createElement('link');
	darkThemeLink.setAttribute('rel', 'stylesheet');
	darkThemeLink.setAttribute('href', 'darkTheme.css');
	document.getElementsByTagName('head')[0].appendChild(darkThemeLink);
    }
});

ipcRenderer.on('load-analyse', (event, analyse: Analyse) => {
    startSimulation(analyse);
});
