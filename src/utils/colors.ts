export const colorsList = [
    "#800000",
    "#006400",
    "#808000",
    "#483d8b",
    "#008b8b",
    "#cd853f",
    "#000080",
    "#32cd32",
    "#8fbc8f",
    "#800080",
    "#b03060",
    "#ff4500",
    "#ff8c00",
    "#00ff00",
    "#9400d3",
    "#dc143c",
    "#00ffff",
    "#00bfff",
    "#0000ff",
    "#adff2f",
    "#da70d6",
    "#ff00ff",
    "#1e90ff",
    "#fa8072",
    "#ffff54",
    "#b0e0e6",
    "#90ee90",
    "#ff1493",
    "#7b68ee",
    "#ffdead",
    "#ffb6c1",
    "#808080",
]

function rgbComponentToHex(c: number) : string {
    let hex = Math.floor(c).toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

export function rgbToHex(r: number, g: number, b: number): string {
    return "#" + rgbComponentToHex(r) + rgbComponentToHex(g) + rgbComponentToHex(b);
}
