import { Node, Link } from './graph'

export interface Filter {
    call: (nodes: Node[], links: Link[]) => {
	nodes: Node[],
	links: Link[],
    },
    buildUI: (parent: HTMLElement, redrawCallback: () => void) => void
};

export class MinInteractionsCountFilter implements Filter {

    interactionsCount: number;

    constructor(interactionsCount: number = 50) {
	this.interactionsCount = interactionsCount;
    }

    call(nodes: Node[], links: Link[]): {nodes: Node[], links: Link[]} {
	return {
	    nodes: nodes.filter((node) => {
		return node.interactionsCount >= this.interactionsCount;
	    }),
	    links: links.filter((link) => {
		return (
		    link.target.interactionsCount >= this.interactionsCount
			&& link.source.interactionsCount >= this.interactionsCount
		);
	    })
	};
    }

    buildUI(parent: HTMLElement, redrawCallback: () => void) {
	let interactionsCountLabel = document.createElement("label");
	interactionsCountLabel.textContent = "Interactions count";
	parent.appendChild(interactionsCountLabel);

	let interactionsCountInput = document.createElement("input") as HTMLInputElement;
	interactionsCountInput.setAttribute("type", "text");
	interactionsCountInput.value = this.interactionsCount.toString();
	interactionsCountInput.addEventListener("input", (event: any) => {
	    this.interactionsCount = event.target.value;
	    redrawCallback();
	});
	parent.appendChild(interactionsCountInput);
    }
};

export class NameRegexFilter implements Filter {

    regex: string;

    constructor(regex: string = "^$") {
	this.regex = regex;
    }

    call(nodes: Node[], links: Link[]): {nodes: Node[], links: Link[]} {
	const regex = new RegExp(this.regex);
	return {
	    nodes: nodes.filter((node) => {
		return !regex.test(node.id);
	    }),
	    links: links.filter((link) => {
		return (
		    !regex.test(link.source.id)
			&& !regex.test(link.target.id)
		);
	    })
	};
    }

    buildUI(parent: HTMLElement, redrawCallback: () => void) {
	let regexLabel = document.createElement("label");
	regexLabel.textContent = "Regex";
	parent.appendChild(regexLabel);

	let regexInput = document.createElement("input") as HTMLInputElement;
	regexInput.setAttribute("type", "text");
	regexInput.value = this.regex.toString();
	regexInput.addEventListener("input", (event: any) => {
	    this.regex = event.target.value;
	    redrawCallback();
	});
	parent.appendChild(regexInput);
    }
}

export class TopKInteractionsCountFilter implements Filter {

    k: number;

    constructor(k: number = 10) {
	this.k = k;
    }

    call(nodes: Node[], links: Link[]): {nodes: Node[], links: Link[]} {
	let topNodes = nodes.sort((a: Node, b: Node) => {
	    return b.interactionsCount - a.interactionsCount;
	}).slice(0, this.k);
	let topIdSet = new Set(topNodes.map((node: Node) => {node.id}));
	let topNodesSet = new Set(topNodes);
	return {
	    nodes: topNodes,
	    links: links.filter((link) => {
		return topNodesSet.has(link.target) && topNodesSet.has(link.source);
	    })
	};
    }

    buildUI(parent: HTMLElement, redrawCallback: () => void) {
	let kLabel = document.createElement("label");
	kLabel.textContent = "K";
	parent.appendChild(kLabel);

	let kInput = document.createElement("input") as HTMLInputElement;
	kInput.setAttribute("type", "text");
	kInput.value = this.k.toString();
	kInput.addEventListener("input", (event: any) => {
	    this.k = event.target.value;
	    redrawCallback();
	});
	parent.appendChild(kInput);
    }
}


export const allFilterTypes = [TopKInteractionsCountFilter, MinInteractionsCountFilter, NameRegexFilter];
