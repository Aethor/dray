export type AnalyseCharacter = {
    name: string,
    interactionsCount: number
};

export type Interaction = {
    activeCharacter: string,
    targetCharacters: Array<{
	name: string,
	influence: number
    }>
};

export type Analyse = {
    characters: Array<{
	name: string,
	interactionsCount: number
    }>,
    interactions: Array<Interaction>
};
